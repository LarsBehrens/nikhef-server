import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets, linear_model

data = pd.read_csv('./data/reg-data.csv', error_bad_lines=False, encoding="Latin 1", delimiter=",")

x = np.array(data.activity)
y = np.array(data.speed)

x.shape +=(1,)

regr = linear_model.LinearRegression()

regr.fit(x, y)

print(regr.coef_)

plt.scatter(x, y)
plt.plot(x,regr.predict(x))

plt.show()

