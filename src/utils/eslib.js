var Elasticsearch = require('elasticsearch');
var Config = require('../config');

var client;

module.exports.getClient = function() {
	if (client) {
		return client;
	} else {
		var client = new Elasticsearch.Client({
			host: Config.esUrl,
			log: 'debug',
			apiVersion: '2.1',
			requestTimeout: 60000000
		});

		return client;
	}
}