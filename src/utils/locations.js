var geoip = require('geoip-lite');

function addLocationAndFormat(elem) {
	var size = parseFloat((elem._source.nbytes_size / 1000000).toFixed(2));
	var host = {};
	var hostLoc = geoip.lookup(elem._source.host);
	var destination = {};
	var destLoc = geoip.lookup(elem._source.destination);

	if (hostLoc && destLoc) {
		host.ll = hostLoc.ll;
		host.ip = elem._source.host;
		var hostString = host.ll[0].toString() + ',' + host.ll[1].toString();

		destination.ll = destLoc.ll;
		destination.ip = elem._source.destination;
		var destString = destination.ll[0].toString() + ',' + destination.ll[1].toString();

		var result = {
			host: host,
			destination: destination,
			totalSize: size,
			frequency: 1
		}

		return {formattedLog: result, hostString, destString};
	} else {
		return null;
	}
}

module.exports.addLocationsAndFormatLogs = function(logs) {
	var uniqueIPLogs = {};
	var uniqueLocationLogsList = [];
	var uniqueLocationLogs = {};

	logs.map(function(elem) {
		var result = addLocationAndFormat(elem);
		var formattedLog = result.formattedLog;
		var hostString = result.hostString;
		var destString = result.destString;

		if (!uniqueLocationLogs[hostString]) {
			uniqueLocationLogs[hostString] = {};
			uniqueLocationLogs[hostString][destString] = formattedLog;
		} else {
			if (!uniqueLocationLogs[hostString][destString]) {
				uniqueLocationLogs[hostString][destString] = formattedLog;
			} else {
				uniqueLocationLogs[hostString][destString].totalSize += formattedLog.totalSize;
				uniqueLocationLogs[hostString][destString].frequency++;
			}
		}

		if (!uniqueIPLogs[elem._source.host]) {
			uniqueIPLogs[elem._source.host] = {};
			uniqueIPLogs[elem._source.host][elem._source.destination] = formattedLog;
		} else {
			if (!uniqueIPLogs[elem._source.host][elem._source.destination]) {
				uniqueIPLogs[elem._source.host][elem._source.destination] = formattedLog;
			} else {
				uniqueIPLogs[elem._source.host][elem._source.destination].totalSize += formattedLog.totalSize;
				uniqueIPLogs[elem._source.host][elem._source.destination].frequency++;
			}
		}
	});

	for (var hostKey in uniqueLocationLogs) {
		for (var destKey in uniqueLocationLogs[hostKey]) {
			uniqueLocationLogsList.push(uniqueLocationLogs[hostKey][destKey]);
		}
	}

	return {
		uniqueLocationLogsList,
		uniqueIPLogs
	};
}