var EsLib = require('./eslib');
var Locations = require ('./locations');

module.exports.EsLib = EsLib;
module.exports.Locations = Locations;

module.exports.getIndexFromDates = function(from, to) {
	var index = 'nikhef-';
	
	var dateFrom = new Date(from);
	var dateTo = new Date(to);

	if (dateFrom.getFullYear() === dateTo.getFullYear()) {
		index += dateFrom.getFullYear().toString() + '.';
		if (dateFrom.getMonth() === dateTo.getMonth()) {
			index += ('0' + (dateFrom.getMonth() + 1).toString()).slice(-2) + '.';
			if (dateFrom.getDate() === dateTo.getDate()) {
				index += ('0' + dateFrom.getDate().toString()).slice(-2);
			} else {
				index += '*';
			}
		} else {
			index += '*';
		}
	} else {
		index += '*';
	}
	return index;
}