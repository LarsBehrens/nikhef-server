var Boom = require('boom');
var Utils = require('../../utils');
var EsLib = Utils.EsLib;
var PythonShell = require('python-shell');
var CSV = require('fast-csv');

module.exports.applyRegression = function(request, reply) {
	var speed = [];
	var EsClient = EsLib.getClient();
	var dateFrom = request.query.from;
	var dateTo = request.query.to;
	var interval = request.query.interval;
	var min_activity = request.query.min_activity;
	var host = request.params.host;
	var index = Utils.getIndexFromDates(dateFrom, dateTo);

	var pythonOptions = {
		mode: 'text',
		pythonPath: 'python',
		pythonOptions: ['-u'],
		scriptPath: './src/python',
		args: []
	}

	EsClient.search({
		index: index,
		size: 0,
		body: {
			aggs: {
				filter_date_transfer: {
					filter: {
						and: [{
							term: {
								msg_type: "transfer"
							}
						}, {
							term: {
								host: host
							}
						}, {
							range: {
								"@timestamp": {
									gte: dateFrom,
									lte: dateTo
								}
							}
						}]
					},
					aggs: {
						per_hour_agg: {
							date_histogram: {
								field: "@timestamp",
								interval: interval,
								min_doc_count: min_activity
							},
							aggs: {
								speed_agg: {
									avg: {
										script: "(doc['nbytes_size'].value/1000000) / (((doc['end_timestamp'].value  - doc['start_timestamp'].value)/1000) != 0.0 ? ((doc['end_timestamp'].value  - doc['start_timestamp'].value)/1000) : 0.001)",
										lang: "expression"
									}
								}
							}
						}
					}
				}
			}
		}
	}).then(function(resp) {
		speed = resp.aggregations.filter_date_transfer.per_hour_agg.buckets;
		console.log(speed);
		var data = processData(speed);

		CSV.writeToPath('./data/reg-data.csv', data, {
			headers: ['activity', 'speed']
		}).on('finish', function() {

			PythonShell.run('regression.py', pythonOptions, function(err, results) {
				if (err) {
					return reply(Boom.badRequest('No data found in this period with the given minimum activity.'));
				}
				// results is an array consisting of messages collected during execution
				console.log('results: %j', results);
				var slope = results[0].substring(1, results[0].length - 1);
				reply({
					slope: slope
				});
			});
		});
	}).catch(function(err) {
		console.log(err);
		reply('Error occured. Check with the developers').code(500);
	});
}

function processData(data) {
	var result = []
	data.map(function(elem) {
		if (elem.speed_agg.value) {
			result.push({
				activity: elem.doc_count,
				speed: elem.speed_agg.value
			});
		}
	});
	return result;
}