var Activity = require('./activity');
var Speed = require('./speed');
var Retrieves = require('./retrieves');
var Stores = require('./stores');
// var All = require('./all');
var Regression = require('./regression');


module.exports.getActivity = Activity.getActivity;
module.exports.getActivityFSize = Activity.getActivityFSize;
module.exports.getActivityPerDest = Activity.getActivityPerDest;
module.exports.getSpeed = Speed.getSpeed;
module.exports.getStores = Stores.getStores;
module.exports.getRetrieves = Retrieves.getRetrieves;
// module.exports.getAll = All.getAll;
module.exports.applyRegression = Regression.applyRegression;