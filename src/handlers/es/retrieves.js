var Boom = require('boom');
var Utils = require('../../utils');
var EsLib = Utils.EsLib;
var geoip = require('geoip-lite');
var Locations = Utils.Locations;

module.exports.getRetrieves = function(request, reply) {
	var logs = {locs: [], hosts: []};
	var logsCount = 0;
	var EsClient = EsLib.getClient();
	var dateFrom = request.query.from;
	var dateTo = request.query.to;
	var index = Utils.getIndexFromDates(dateFrom, dateTo);
	EsClient.search({
		index: index,
		from: 0,
		size: 10000,
		searchType: 'scan',
		scroll: '1m',
		body: {
			filter: {
				and: [{
					term: {
						transfer_type: 'retr'
					}
				}, {
					not: {
						prefix: {
							destination: "0.0.0.0"
						}
					}
				}, {
					range: {
						'@timestamp': {
							gte: dateFrom,
							lte: dateTo
						}
					}
				}]
			}
		}
	}, function processResults(err, response) {
		if (err) {
			console.log(err);
			reply(Boom.badImplementation());
		} else {
			logsCount += response.hits.hits.length;

			var filtered = Locations.addLocationsAndFormatLogs(response.hits.hits);
			logs.locs = logs.locs.concat(filtered.uniqueLocationLogsList);
			logs.hosts = logs.hosts.concat(filtered.uniqueIPLogs);

			if (response.hits.total !== logsCount) {

				EsClient.scroll({
					scrollId: response._scroll_id,
					scroll: '1m'
				}, processResults);

			} else {
				reply(logs);
			}
		}
	});
}
