var Boom = require('boom');
var Utils = require('../../utils');
var EsLib = Utils.EsLib;

module.exports.getSpeed = function(request, reply) {
	var EsClient = EsLib.getClient();
	var dateFrom = request.query.from;
	var dateTo = request.query.to;
	var interval = request.query.interval;
	var index = Utils.getIndexFromDates(dateFrom, dateTo);
	EsClient.search({
		index: index,
		size: 0,
		body: {
			aggs: {
				filter_date_transfer: {
					filter: {
						and: [{
							term: {
								msg_type: "transfer"
							}
						}, {
							range: {
								"@timestamp": {
									gte: dateFrom,
									lte: dateTo
								}
							}
						}]
					},
					aggs: {
						host_agg: {
							terms: {
								field: "host",
								size: 100
							},
							aggs: {
								per_hour_agg: {
									date_histogram: {
										field: "@timestamp",
										interval: interval,
										min_doc_count: 0
									},
									aggs: {
										speed_agg: {
											avg: {
												script: "(doc['nbytes_size'].value/1000000) / (((doc['end_timestamp'].value  - doc['start_timestamp'].value)/1000) != 0.0 ? ((doc['end_timestamp'].value  - doc['start_timestamp'].value)/1000) : 0.001)",
												lang: "expression"
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}).then(function(resp) {
		if(resp.aggregations) {
		reply(resp.aggregations.filter_date_transfer.host_agg.buckets);
		} else {
			reply(Boom.badRequest('No data found in this period.'));
		}
	}).catch(function(err) {
		console.log(err);
		reply(Boom.badImplementation());
	});
}
