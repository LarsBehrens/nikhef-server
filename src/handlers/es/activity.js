var Boom = require('boom');
var Utils = require('../../utils');
var EsLib = Utils.EsLib;

module.exports.getActivity = function(request, reply) {
	var EsClient = EsLib.getClient();
	var dateFrom = request.query.from;
	var dateTo = request.query.to;
	var interval = request.query.interval;
	var index = Utils.getIndexFromDates(dateFrom, dateTo);
	EsClient.search({
		index: index,
		size: 0,
		body: {
			aggs: {
				filter_date_transfer: {
					filter: {
						and: [{
							term: {
								msg_type: "transfer"
							}
						}, {
							range: {
								'@timestamp': {
									gte: dateFrom,
									lte: dateTo
								}
							}
						}]
					},
					aggs: {
						host_agg: {
							terms: {
								field: "host",
								size: 10000
							},
							aggs: {
								per_hour_agg: {
									date_histogram: {
										field: "@timestamp",
										interval: interval,
										min_doc_count: 0
									}
								}
							}
						}
					}
				}
			}
		}
	}).then(function(resp) {
		if(resp.aggregations) {
		reply(resp.aggregations.filter_date_transfer.host_agg.buckets);
		} else {
			reply(Boom.badRequest('No data found in this period.'));
		}
	}).catch(function(err) {
		console.log(err);
		reply(Boom.badImplementation());
	});
}

module.exports.getActivityFSize = function(request, reply) {
	var EsClient = EsLib.getClient();
	var dateFrom = request.query.from;
	var dateTo = request.query.to;
	var interval = request.query.interval;
	var index = Utils.getIndexFromDates(dateFrom, dateTo);
	EsClient.search({
		index: index,
		size: 0,
		body: {
			aggs: {
				filter_date_transfer: {
					filter: {
						and: [{
							term: {
								msg_type: "transfer"
							}
						}, {
							range: {
								'@timestamp': {
									gte: dateFrom,
									lte: dateTo
								}
							}
						}]
					},
					aggs: {
						host_agg: {
							terms: {
								field: "host",
								size: 10000
							},
							aggs: {
								size_agg: {
									sum: {
										script: "doc['nbytes_size'].value/1000000",
										lang: 'expression'
									}
								}
							}
						}
					}
				}
			}
		}
	}).then(function(resp) {
		if(resp.aggregations) {
		reply(resp.aggregations.filter_date_transfer.host_agg.buckets);
		} else {
			reply(Boom.badRequest('No data found in this period.'));
		}
	}).catch(function(err) {
		console.log(err);
		reply(Boom.badImplementation());
	});
}

module.exports.getActivityPerDest = function(request, reply) {
	var EsClient = EsLib.getClient();
	var dateFrom = request.query.from;
	var dateTo = request.query.to;
	var interval = request.query.interval;
	var host = request.params.host;
	var index = Utils.getIndexFromDates(dateFrom, dateTo);
	EsClient.search({
		index: index,
		size: 0,
		body: {
			aggs: {
				filter_date_transfer: {
					filter: {
						and: [{
							term: {
								msg_type: "transfer"
							}
						}, {
							range: {
								'@timestamp': {
									gte: dateFrom,
									lte: dateTo
								}
							}
						}, {
							term: {
								host: host
							}
						}]
					},
					aggs: {
						dest_agg: {
							terms: {
								field: "destination",
								size: 10000
							},
							aggs: {
								per_hour_agg: {
									date_histogram: {
										field: "@timestamp",
										interval: interval
									}
								}
							}
						}
					}
				}
			}
		}
	}).then(function(resp) {
		if(resp.aggregations) {
		reply(resp.aggregations.filter_date_transfer.host_agg.buckets);
		} else {
			reply(Boom.badRequest('No data found in this period.'));
		}
	}).catch(function(err) {
		console.log(err);
		reply(Boom.badImplementation());
	});
}