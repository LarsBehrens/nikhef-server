const Handlers = require('./handlers');
const Joi = require('joi');


module.exports = [{
		method: 'GET',
		path: '/es/transfer/activity',
		handler: Handlers.Es.getActivity,
		config: {
			cors: true,
			validate: {
				query: {
					from: Joi.date().iso().required(),
					to: Joi.date().iso().required(),
					interval: Joi.string().regex(/^(hour|minute)$/).required()
				}
			}
		}
	}, {
		method: 'GET',
		path: '/es/transfer/activity/fs',
		handler: Handlers.Es.getActivityFSize,
		config: {
			cors: true,
			validate: {
				query: {
					from: Joi.date().iso().required(),
					to: Joi.date().iso().required()
				}
			}
		}
	}, {
		method: 'GET',
		path: '/es/transfer/activity/{host}/dest',
		handler: Handlers.Es.getActivityPerDest,
		config: {
			cors: true,
			validate: {
				params: {
					host: Joi.string().ip()
				},
				query: {
					from: Joi.date().iso().required(),
					to: Joi.date().iso().required(),
					interval: Joi.string().regex(/^(hour|minute)$/).required()
				}
			}
		}
	}, {
		method: 'GET',
		path: '/es/transfer/retrieves',
		handler: Handlers.Es.getRetrieves,
		config: {
			cors: true,
			validate: {
				query: {
					from: Joi.date().iso().required(),
					to: Joi.date().iso().required()
				}
			}
		}
	}, {
		method: 'GET',
		path: '/es/transfer/stores',
		handler: Handlers.Es.getStores,
		config: {
			cors: true,
			validate: {
				query: {
					from: Joi.date().iso().required(),
					to: Joi.date().iso().required()
				}
			}
		}
	},
	// {
	// 	method: 'GET',
	// 	path: '/es/transfer/all',
	// 	handler: Handlers.Es.getAll,
	// 	config: {
	// 		cors: true,
	// 		validate: {
	// 			query: {
	// 				from: Joi.date().iso().required(),
	// 				to: Joi.date().iso().required()
	// 			}
	// 		}
	// 	}
	// },
	{
		method: 'GET',
		path: '/es/transfer/speed',
		handler: Handlers.Es.getSpeed,
		config: {
			cors: true,
			validate: {
				query: {
					from: Joi.date().iso().required(),
					to: Joi.date().iso().required(),
					interval: Joi.string().regex(/^(hour|minute)$/).required()
				}
			}
		}
	}, {
		method: 'GET',
		path: '/es/transfer/speed/{host}/lregression',
		handler: Handlers.Es.applyRegression,
		config: {
			cors: true,
			validate: {
				params: {
					host: Joi.string().ip().required()
				},
				query: {
					from: Joi.date().iso().required(),
					to: Joi.date().iso().required(),
					interval: Joi.string().regex(/^(hour|minute)$/).required(),
					min_activity: Joi.number().required()
				}
			}
		}
	}
];