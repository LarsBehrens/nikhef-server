'use strict'

const Hapi = require('hapi');
const Routes = require('./src/routes');

// Create a server with a host and port
const server = new Hapi.Server();
server.connection({
	port: 3000
});

// Add the routes
server.route(Routes);

// Start the server
server.start((err) => {
	if (err) {
		throw err;
	};
	console.log('Server running at:', server.info.uri);
});