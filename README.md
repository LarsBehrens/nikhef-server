# ROUTES

Start command: "npm start" in de root van de folder

Base url: localhost:3000

- GET /es/transfer/activity. Query parameters: from(ISO date), to(ISO data), interval(hour, minute etc.)
	bv: http://localhost:3000/es/transfer/activity?from=2016-04-29T00:00:00.000Z&to=2016-04-30T00:00:00.000Z&interval=hour

- GET /es/transfer/retrieves. Query parameters: from(ISO date), to(ISO data)
	bv: http://localhost:3000/es/transfer/retrieves?from=2016-04-29T00:00:00.000Z&to=2016-05-30T00:00:00.000Z

## notes
- geo-ip lite moet eigenlijk elke maand een script runnen voor db update.